#include "stdafx.h"
#include "GameBoard.h"
#include <windows.h>

void main()
{
	SnakeGame::GameBoard gameBoard;

	while (!gameBoard.IsExit())
	{
		gameBoard.MakeMovement();
	}

	getchar();
}
